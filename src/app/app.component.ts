import { Component, ViewChild } from '@angular/core';
import { Platform, Content } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { WelcomePage } from '../pages/welcome/welcome'
import { TabsPage } from '../pages/tabs/tabs';


import { MenuPage } from '../pages/menu/menu'
import { ParentPage } from '../pages/parent/parent'
import { GraphPage } from '../pages/graph/graph'

import { requestService } from '../services/request'
import { SharedService } from '../services/sharedService'


@Component({
  templateUrl : 'app.html'
})
 
export class MyApp {
  rootPage:any = MenuPage; 
  @ViewChild(Content) content: Content;
  private dataProfile = {Name: '-', Title: '-', Surname: '-'}

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen, 
    private _requestService : requestService,
    private _SharedService : SharedService,
  ) {
    platform.ready().then(() => {
      statusBar.backgroundColorByHexString('#2ab363');
      splashScreen.hide();  
    });

    _requestService.subscript().subscribe(obj => {
      if(obj.type === 'signin') { 
        this.dataProfile = obj.profile
      }
    })
  }

  ngAfterViewInit() {
    this.content.resize();
  }

  logout() {
    const logoutEvent = () => {
      this._SharedService.presentAlert(2, '')
      this._requestService.logout()
    }
    this._SharedService.presentConfirm('ออกจากระบบ', 'คุณต้องการออกจากระบบหรือไม่', 'ออกจากระบบ', logoutEvent)
  }
}
