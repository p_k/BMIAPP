import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { BrowserTab } from '@ionic-native/browser-tab';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule, Color } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { NativeStorage } from '@ionic-native/native-storage';



import { AboutPage } from '../pages/about/about'; 
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { WelcomePage } from '../pages/welcome/welcome'
import { MenuPage } from '../pages/menu/menu'
import { ParentPage } from '../pages/parent/parent'
import { GraphPage } from '../pages/graph/graph'
import { ConditionPage} from '../pages/condition/condition'

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { bodyJson } from '../pages/home/body-style-data'
 
// import service
import { CalculatorService } from '../services/calculator'
import { SharedService } from '../services/sharedService'
import { requestService } from '../services/request'
 
//import pipe 

import { DirectiveModule } from '../directive/index'
import { PipeModule } from '../pipes/pipe'

@NgModule({
  declarations: [ 
    MyApp,
    AboutPage,
    ContactPage, 
    HomePage, 
    TabsPage,
    WelcomePage,
    MenuPage,
    ParentPage,
    GraphPage,
    ConditionPage
  ],
  imports: [
    SuperTabsModule.forRoot(), 
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      monthNames: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],
    }),
    ChartsModule,   
    NgxChartsModule,
    BrowserAnimationsModule,
    DirectiveModule,
    HttpModule,
    PipeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,  
    ContactPage,
    HomePage,
    TabsPage,
    WelcomePage, 
    MenuPage,
    ParentPage,
    GraphPage,
    ConditionPage
  ],
  providers: [
    bodyJson, 
    StatusBar, 
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CalculatorService,
    SharedService,
    requestService,
    SpinnerDialog,
    NativeStorage,
    BrowserTab
  ]
})
export class AppModule {}
