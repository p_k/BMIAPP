import { NgModule }      from '@angular/core';
import { commaNumber }  from './inputnumber';
import { inputNumber } from './inputcomma'
@NgModule({
    imports:        [],
    declarations:   [commaNumber, inputNumber], 
    exports:        [commaNumber, inputNumber],
})

export class DirectiveModule { 
    static forRoot() {
        return {
            ngModule: DirectiveModule,
            providers: [],
        };
    }
}