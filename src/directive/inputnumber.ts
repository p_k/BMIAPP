import { Directive, ElementRef, HostListener, EventEmitter, Output } from '@angular/core';
import { NgControl, NgModel, ControlValueAccessor } from '@angular/forms';


@Directive({
    selector: '[commaNumber]', 
    providers: [ NgModel ],
    host: {
        '(ngModelChange)' : 'onInputChange($event)'
    }
}) 

export class commaNumber {
    @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
    private ngControl;

    constructor(private ele: ElementRef,
                private ngModel: NgModel,
                private control: NgControl) {
    }

    ngOnInit() {
        setTimeout(()=> {
            if(typeof this.ele.nativeElement.children[0] !== 'undefined') {
                this.onFocusOut();
            }
        }, 300)
    }

    @HostListener('blur')  onFocusOut() {
        let value =  this.ele.nativeElement.children[0].value.toString() || '';
        value = value.toString().match(/\d+/g);
        if(value !== null) {
            value = Number(value.join(''));
            this.control.valueAccessor.writeValue(
                (isNaN(Number(value)) ? 0 : value.toFixed(0).replace(/(\d)(?=(\d{3})+$)/g, '$1,'))
            );
        }
    }

    onInputChange($event: string = '') {
        let value = $event;
        let valueChahge = value.toString().match(/[0-9|.]/g);
        let valueNumber;
        if(valueChahge !== null) {
            let valueTxt = valueChahge.join('')
            let valueArr = valueTxt.split('.');
            valueNumber = valueArr[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,') ;
            this.control.valueAccessor.writeValue(valueNumber);
        } else {
            this.control.valueAccessor.writeValue('');
        }
    }
}