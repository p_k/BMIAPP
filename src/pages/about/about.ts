import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, Content } from 'ionic-angular';

import { CalculatorService } from './../../services/calculator'
import { SharedService } from './../../services/sharedService'
import { requestService } from '../../services/request'
import { WelcomePage } from '../welcome/welcome'
import { ConditionPage } from '../condition/condition' 

@Component({
  selector: 'page-result',
  templateUrl: 'about.html'
})
export class AboutPage {
  @ViewChild(Content) content: Content;
  private emiValue : any = {
    w: '-',
    h: '-',
    r: '-',
    img: '',
    emiTxt: 'ไม่ระบุ',
    disabled: false,
    detail: '<div>ไม่ระบุ</div>'
  }
  private emi : any = {}
  constructor(
    public navCtrl: NavController,
    private _CalculatorService : CalculatorService,
    private _SharedService : SharedService,
    private _requestService : requestService,
    private modalCtr: ModalController
  ) {
    this.emi = Object.assign({}, this.emiValue)
    _CalculatorService.subscript().subscribe(param => {
      this.emi.w = param.w;
      this.emi.h = param.h;
      this.emi.r = param.r; 
      this.emi.emiTxt = param.name;
      this.emi.disabled = true
      this.emi.img = `assets/icon/${param.style}`
      this._requestService.loadData('get', 
      `https://armysmarthealth.rta.mi.th/Mobile/BMIWebService.ashx?Method=GetAdvice&BMI=${param.r}`,new FormData(), true).then(res => {
        if(typeof res.error !== 'undefined') {
          this.emi.detail = '<div>ไม่ระบุ</div>'
        } else {
          this.emi.detail = `<div>${res.Message}</div>`
        }
      })
    })
  }

  openModal() {
    let conditionModal = this.modalCtr.create(ConditionPage);
    conditionModal.present();
  }

  reset() {
    // this.emi = Object.assign({}, this.emiValue)
  }

  ngAfterViewInit() {
    this.content.resize();
  }

  submit() {
    let openModalSignin = () => {
      let graphModal = this.modalCtr.create(WelcomePage);
      graphModal.present();
    }

    if(this.emi.disabled) {
      if(!this._requestService.Status) {
        this._SharedService.presentConfirm('กรุณาเข้าสู่ระบบ', 'กรุณาเข้าสู่ระบบก่อนทำรายการนี้', 'เข้าสู่ระบบ', openModalSignin)
      } else {
        var formdata = new FormData();
        formdata.append("Height", this.emi.h)
        formdata.append("Weight", this.emi.w)
        formdata.append("Method", 'SaveBMI')
        this._requestService.loadData('post', 'https://armysmarthealth.rta.mi.th/Mobile/BMIWebService.ashx',formdata).then((res) => {
          if(res.ErrorCode == 0) {
            this._requestService.onUpdateAuthToken(res.Token);
            this._SharedService.presentAlertCustom('บันทึกสำเร็จ', '');
            this.reset();
          } else {
            this._SharedService.presentAlertCustom(res.ErrorMessage);
          }
        })
      }
    } else {
      this._SharedService.presentAlertCustom('กรุณาคำนวณค่า emi ก่อนบันทึก');
    }
  }

}
