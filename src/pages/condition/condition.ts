import { Component, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup, NG_VALUE_ACCESSOR, FormControl } from '@angular/forms';
import { NavController, Content, ViewController } from 'ionic-angular';
import { CalculatorService } from './../../services/calculator'
import { SharedService } from './../../services/sharedService'
import { TabsPage } from '../tabs/tabs' 
import { bodyJson } from '../home/body-style-data'

@Component({ 
  selector: 'page-condition', 
  templateUrl: 'condition.html'
}) 
 
export class ConditionPage {
  private data : any = this._bodyJson.data
  private factor : any = {}
  private calculateFrom : any; 
  @ViewChild(Content) content: Content;

  constructor(
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    private _bodyJson : bodyJson, 
    private _CalculatorService : CalculatorService,
    private _SharedService : SharedService,
    public viewCtrl: ViewController,
  ) {

  }
  ngAfterViewInit() {
    this.content.resize();
  }
  dismiss() {
    this.viewCtrl.dismiss({});
}

}
