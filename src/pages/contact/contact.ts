import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, Content, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, NG_VALUE_ACCESSOR, FormControl } from '@angular/forms';
import { SharedService } from './../../services/sharedService'
import { requestService } from '../../services/request'
import { GraphPage } from '../graph/graph'
import { WelcomePage } from '../welcome/welcome'

@Component({
  selector: 'page-contact', 
  templateUrl: 'contact.html'
})
export class ContactPage {
  private reportForm : any; 
  private startDate : string = ''; 
  private endDate : string = '';
  private data : any = [];
  private editStatus : boolean = false; 
  private accessibility : boolean = false;
  private startmaxdate;
  private startmindate;
  private dataProfile = {Name: '-', Title: '-', Surname: '-'}
  @ViewChild(Content) content: Content;
  constructor(
    public navCtrl: NavController,
    private _SharedService : SharedService,
    private formBuilder: FormBuilder,
    private _requestService : requestService,
    private modalCtr: ModalController,
    private _AlertController : AlertController
  ) {
    this.initCardModel();
    _requestService.subscript().subscribe(obj => {
      if(obj.type === 'signin') {
        this.accessibility = obj.value
        this.dataProfile = obj.profile
      }
    })
  }


  dateStartChange() {
    if(this.startDate !== '') {
      this.startmindate = this.startDate
    }else {
      this.startmindate = undefined
    }
  }

  dateEndChange() {
    if(this.endDate !== '') {
      this.startmaxdate = this.endDate
    } else {
      this.startmaxdate = undefined
    }
  }

  actionEdit() {
    this.editStatus = this.editStatus === false;
  }

  ngAfterViewInit() {
    this._requestService.checkStatus()
    this.content.resize();
  }

  delete(index, objectKey) {
    let alert = this._AlertController.create({
      title: 'ยืนยันการทำรายการ',
      message: 'คุณต้องการลบรายการนี้หรือไม่',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel'
        },
        {
          text: 'ลบ',
          handler: () => {
            var formdata = new FormData();
            formdata.append("ID", index)
            formdata.append("Method", 'DeleteBMI')
            this._requestService.loadData('post', 'https://armysmarthealth.rta.mi.th/Mobile/BMIWebService.ashx',formdata).then((res) => {
              if(res.ErrorCode == 0) {
                this._requestService.onUpdateAuthToken(res.Token);
                this.data.splice(objectKey, 1);
                this._SharedService.presentAlertCustom('ลบรายการเรียบร้อยแล้ว', '');
              } else {
                this._SharedService.presentAlertCustom(res.ErrorMessage);
              }
            })
          }
        }
      ]
    });
    alert.present();
  }

  login() {
    let graphModal = this.modalCtr.create(WelcomePage);
    graphModal.present();
  }


  openGraph() {
    if (this.reportForm.valid) {
      let graphModal = this.modalCtr.create(GraphPage, { data: this.data, startDate: this.startDate, endDate: this.endDate, dataProfile: this.dataProfile });
      graphModal.present();
    } else {
      this._SharedService.presentAlert();
    }
  }

  initCardModel() {
    let model = {
      startDate: [this.startDate, [Validators.required]],
      endDate: [this.endDate, [Validators.required]]
    };

    this.reportForm = this.formBuilder.group(model);
  }

  reset() {
    this.startmindate = undefined
    this.startmaxdate = undefined
    this.data = [];
    this.startDate = '';
    this.endDate = '';
  }

  submit = () => {
    if (this.reportForm.valid) {
      var formdata = new FormData();
      formdata.append("StartDate", this.startDate)
      formdata.append("EndDate", this.endDate)
      formdata.append("Method", 'ListBMI')
      this._requestService.loadData('post', 'https://armysmarthealth.rta.mi.th/Mobile/BMIWebService.ashx',formdata).then((res) => {
        if(res.ErrorCode == 0) {
          this._requestService.onUpdateAuthToken(res.Token);
          let value = 0;
          this.data = res.Records.reverse().map(obj => {
            if(Number(obj.BMI) > Number(value)) {
              obj.arrow = true
              obj.color = 'red'
            } else {
              obj.arrow = false
              obj.color = 'green'
            }
            if(obj.Diff == 0) {
              obj.color = 'green'
            }
            value = obj.BMI;
            return obj;
          })
          this.data.reverse()
        } else {
          this._SharedService.presentAlertCustom(res.ErrorMessage);
        }
      })
      
    } else {
      this._SharedService.presentAlert();
    }
  } 

}
