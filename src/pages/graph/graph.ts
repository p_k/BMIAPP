import { Component, ViewChild, ElementRef} from '@angular/core';
import { NavController, NavParams, ViewController, ModalController, Content, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, NG_VALUE_ACCESSOR, FormControl } from '@angular/forms';

import { BaseChartDirective, Color } from 'ng2-charts';
import { requestService } from '../../services/request'
import { SharedService } from './../../services/sharedService'

@Component({
  selector: 'page-graph', 
  templateUrl: 'graph.html'
})
export class GraphPage {
  @ViewChild(BaseChartDirective) mainChart: BaseChartDirective;
  @ViewChild('modal') el:ElementRef;
  @ViewChild(Content) content: Content;
  private graphForm : any;
  private startmaxdate;
  private startmindate;
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(16,130,31,0.15)',
      borderColor: 'green',
      pointBackgroundColor: 'green',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartData:Array<any> = [
    {
      backgroundColor: 'rgba(16,130,31,0.15)',
      borderColor: 'green',
      pointBackgroundColor: 'rgba(148,159,177,0.8)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)',
      data: [18, 48, 77, 9, 50, 27, 40]
      , 
      label: 'bmi'}
  ];
  private dataProfile = {Name: '-', Title: '-', Surname: '-'}
  private startDate : string = '';
  private lastStartDate : string = '';
  private endDate : string = '';
  private lastEndDate : string = '';
  private data : any = [];
  private editStatus : boolean = false;
  public lineChartLabels:Array<any> = [];
  public lineChartOptions:any = {
    responsive: true
  };
  private test = 'red'


  constructor(
    public navCtrl: NavController, 
    public navParam: NavParams,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder,
    private _requestService : requestService,
    private _SharedService : SharedService,
    private modalCtr: ModalController,
    private _AlertController : AlertController
  ) {

    _requestService.subscript().subscribe(obj => {
      if(obj.type === 'signin') {
        this.dataProfile = obj.profile
      }
    })

    this.data = this.navParam.data.data || this.data;
    this.startDate = this.navParam.data.startDate || this.startDate;
    this.endDate = this.navParam.data.endDate || this.endDate;
    this.lastStartDate = this.navParam.data.startDate || this.lastStartDate;
    this.lastEndDate = this.navParam.data.endDate || this.lastEndDate;
    this.dataProfile = this.navParam.data.dataProfile || this.dataProfile;
    this.initCardModel();
  }


  actionEdit() {
    this.editStatus = this.editStatus === false;
  }

  dateStartChange() {
    if(this.startDate !== '') {
      this.startmindate = this.startDate
    }else {
      this.startmindate = undefined
    }
  }

  dateEndChange() {
    if(this.endDate !== '') {
      this.startmaxdate = this.endDate
    } else {
      this.startmaxdate = undefined
    }
  }

  delete(index, objectKey) {
    let alert = this._AlertController.create({
      title: 'ยืนยันการทำรายการ',
      message: 'คุณต้องการลบรายการนี้หรือไม่',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel'
        },
        {
          text: 'ลบ',
          handler: () => {
            var formdata = new FormData();
            formdata.append("ID", index)
            formdata.append("Method", 'DeleteBMI')
            this._requestService.loadData('post', 'https://armysmarthealth.rta.mi.th/Mobile/BMIWebService.ashx',formdata).then((res) => {
              if(res.ErrorCode == 0) {
                this._requestService.onUpdateAuthToken(res.Token);
                this.data.splice(objectKey, 1);
                this._SharedService.presentAlertCustom('ลบรายการเรียบร้อยแล้ว', '');
              } else {
                this._SharedService.presentAlertCustom(res.ErrorMessage);
              }
            })
          }
        }
      ]
    });
    alert.present();
  }
  public lineChartLegend:boolean = false;
  public lineChartType:string = 'line';

  openModal() {
    this.el.nativeElement.style.display = 'flex';
  }

  initCardModel() {
    let model = {
      startDate: [this.startDate, [Validators.required]],
      endDate: [this.endDate, [Validators.required]]
    };

    this.graphForm = this.formBuilder.group(model);
  }

  modalDismiss() {
    this.el.nativeElement.style.display = 'none';
    this.startDate = this.lastStartDate;
    this.endDate = this.lastEndDate;
  }

  submit = () => {
    if (this.graphForm.valid) {
      var formdata = new FormData();
      formdata.append("StartDate", this.startDate)
      formdata.append("EndDate", this.endDate)
      formdata.append("Method", 'ListBMI')
      this._requestService.loadData('post', 'https://armysmarthealth.rta.mi.th/Mobile/BMIWebService.ashx',formdata).then((res) => {
        if(res.ErrorCode == 0) {
          this._requestService.onUpdateAuthToken(res.Token);
          let value = 0;
          this.data = res.Records.reverse().map(obj => {
            if(Number(obj.BMI) > Number(value)) {
              obj.arrow = true
              obj.color = 'red'
            } else {
              obj.arrow = false
              obj.color = 'green'
            }
            if(obj.Diff == 0) {
              obj.color = 'green'
            }
            value = obj.BMI;
            return obj;
          })
          this.data.reverse()
          this.lastStartDate = this.startDate;
          this.lastEndDate = this.endDate;
          this.modalDismiss();
          this.reactive();
        } else {
          this._SharedService.presentAlertCustom(res.ErrorMessage);
        }
      })
      
    } else {
      this._SharedService.presentAlert();
    }
  } 
  

  reactive() {
    let lineChart = [];
    this.lineChartData[0].data = [];
    this.data.map((obj, i) => {
      this.lineChartData[0].data.push(obj.BMI);
      lineChart.push(obj.RecordDate.substr(5) + '-' + obj.RecordDate.substr(2,2))
    })
    this.mainChart.chart.config.data.labels = lineChart;
    this.mainChart.chart.config.data.datasets = this.lineChartData;
    this.mainChart.chart.update();
  }
  dismiss() {
    this.viewCtrl.dismiss({});
  }

  ionViewDidEnter(){
    this.reactive();
    this.content.resize();
}

}
