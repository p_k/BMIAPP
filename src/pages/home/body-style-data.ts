import { Injectable } from '@angular/core';

@Injectable()

export class bodyJson { 
  public data : any  = [
    {
      style : 'bmi_man1.png',
      condition : 'ต่ำกว่า < 18.50',
      name : 'น้ำหนักต่ำกว่าเกณฑ์',
      val: 18.5,
      type: 'less'
    },
    {
      style : 'bmi_man2.png',
      condition : '18.50 - 22.99',
      name : 'น้ำหนักปกติ', 
      val: 22.99,
      type: 'less'
    },
    {
      style : 'bmi_man3.png',
      condition : '23.00 - 24.99', 
      name : 'น้ำหนักเกิน',
      val : 24.99,
      type: 'less'
    },
    {
      style : 'bmi_man4.png',
      condition : '25.00 - 29.99',
      name : 'อ้วนระดับ 1',
      val : 29.99,
      type: 'less'
    },
    {
      style : 'bmi_man5.png',
      condition : '> 30 ขึ้นไป',
      name : 'อ้วนระดับ 2',
      val : 30,
      type: 'more'
    }
  ];
}
