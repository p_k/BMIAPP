import { Component, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup, NG_VALUE_ACCESSOR, FormControl } from '@angular/forms';
import { NavController, Content } from 'ionic-angular';
import { bodyJson } from './body-style-data'
import { CalculatorService } from './../../services/calculator'
import { SharedService } from './../../services/sharedService'
import { TabsPage } from '../tabs/tabs' 

@Component({ 
  selector: 'page-home',
  templateUrl: 'home.html'
})
 
export class HomePage {
  private data : any = this._bodyJson.data
  private factor : any = {}
  private calculateFrom : any; 
  @ViewChild(Content) content: Content;

  constructor(
    public navCtrl: NavController,
    private _bodyJson : bodyJson, 
    private formBuilder: FormBuilder,
    private _CalculatorService : CalculatorService,
    private _SharedService : SharedService
  ) {
    this.factor = _CalculatorService.getFactorCal
    this.initCardModel();
  }
  minZero(control: FormControl) {
    const { value } = control
    if(value <= 0) {
      return `can't your put number minimum 0`
    }
  }
  ngAfterViewInit() {
    this.content.resize();
  }
  
  initCardModel() {
    let model = {
      height: [this.factor.height, [Validators.required, this.minZero]],
      weight: [this.factor.weight, [Validators.required, this.minZero]]
    };
    this.calculateFrom = this.formBuilder.group(model);
  }
 
  submit = () => {
    if (this.calculateFrom.valid) {
      this._CalculatorService.calculatorEmi(this.factor);
    } else {
      this._SharedService.presentAlert();
    }
  } 

}
