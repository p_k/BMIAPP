import { Component } from '@angular/core';
import { NavController, ViewController, App } from 'ionic-angular';
import { BrowserTab } from '@ionic-native/browser-tab';
import { ParentPage } from '../parent/parent'

@Component({
    selector: 'page-menu',
    templateUrl: 'menu.html',
})

export class MenuPage {

    constructor(
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        private browserTab: BrowserTab,
        public appCtrl: App
    ) {
        
    }

    openEmi() {
        this.navCtrl.push(ParentPage)
        // this.appCtrl.getRootNav().push(ParentPage);
        // this.navCtrl.rootNav.push(ParentPage);
    }

    openTabBrowser(action: number) {
        switch(action) { 
        case 1:
            this.browserTab.openUrl('https://armysmarthealth.rta.mi.th/ArmyHIS?per=1')
        return true
        case 2: 
            this.browserTab.openUrl('https://armysmarthealth.rta.mi.th/')
        return true
        case 3: 
            this.browserTab.openUrl('https://armysmarthealth.rta.mi.th/ArmyHIS?per=0')
        return true
        default: return true
        }
    }

    ionViewDidLoad(){
        this.viewCtrl.showBackButton(false);
        console.log('ionViewDidLoad RegisterPage');
    }
}
