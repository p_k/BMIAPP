import { Component, ViewChild } from '@angular/core';
import { TabsPage } from './../tabs/tabs'
import { requestService } from '../../services/request'
import { MenuController,NavController, Content } from 'ionic-angular';

@Component({
  templateUrl: 'parent.html'
})
export class ParentPage { 
  private accessibility: boolean = false;
  @ViewChild(Content) content: Content;
  constructor(
    private _requestService: requestService,
    public menuCtrl: MenuController,
    public navCtrl: NavController
  ) {
    _requestService.subscript().subscribe(obj => {
      if (obj.type === 'signin') {
        this.accessibility = obj.value
      }

      if (!obj.value) {
        this.menuCtrl.close(); 
      } 
    })

    
  }

  openMenu() {
    this.menuCtrl.open();
  }

  dismiss() {
    this.navCtrl.pop();
  }

  ionViewDidLoad(){
  
    this.content.resize()
  }
}
