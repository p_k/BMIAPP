import { Component, ViewChild } from '@angular/core';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { SuperTabsController } from 'ionic2-super-tabs'
import { CalculatorService } from '../../services/calculator'
import { SharedService } from '../../services/sharedService'
import { requestService } from '../../services/request'

@Component({
  selector : 'page-tabs',
  templateUrl: 'tabs.html' 
})
export class TabsPage {
 
  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  
  private accessibility : boolean = false;
  private isLoadding : boolean = true;

  constructor(
    private _superTabsCtrl: SuperTabsController,
    private _CalculatorService :CalculatorService,
    private _SharedService :SharedService,
    private _requestService : requestService
  ) {

    _requestService.subscript().subscribe(obj => {
      if(obj.type === 'signin') {
        this.accessibility = obj.value
      }
    })

    

    _CalculatorService.subscript().subscribe(param => {
      const { action } = param
      if(action === 'changeTab') {
        _superTabsCtrl.slideTo(1)
      }
    })
  }
 
  ngAfterViewInit() {
    this._requestService.checkStatus()
  }
}
