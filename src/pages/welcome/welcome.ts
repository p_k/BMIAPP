import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { SharedService } from './../../services/sharedService'
import { requestService } from '../../services/request'
import { ParentPage } from '../parent/parent'

@Component({
    selector: 'page-welcome',
    templateUrl: 'welcome.html',
})

export class WelcomePage {

    private dataProfile : any = {}
    private status : any = []
    private username : string;
    private password : string;
    private authForm : any;

    constructor(
        public navCtrl: NavController,
        private formBuilder: FormBuilder,
        public viewCtrl: ViewController,
        private _SharedService : SharedService,
        private _requestService : requestService
    ) { 
        this.initCardModel();
    }

    initCardModel() {
        let model = {
            username: [this.username, Validators.required],
            password: [this.password, Validators.required]
        };

        this.authForm = this.formBuilder.group(model);
    }

    ionViewDidLoad(){
        console.log('ionViewDidLoad RegisterPage');
    }

    submit = () => {
        var formdata = new FormData();
        formdata.append("Username", this.username)
        formdata.append("Password", this.password)
        formdata.append("Method", 'Login')

        if (this.authForm.valid) {
            this._requestService.request('post', "https://armysmarthealth.rta.mi.th/Mobile/LoginService.ashx", formdata).then(res => {
                if(typeof res.error !== 'undefined' || res.ErrorCode != 0) {
                    this._SharedService.presentAlertCustom('บัญชีผู้ใช้และรหัสผ่านผิด กรุณาลองใหม่อีกครั้ง');
                } else {
                    this._requestService.onUpdateAuthToken(res.Token, this.username).then((cb) => {
                        if(cb) {
                            this._requestService.setProfile(res);
                            this._requestService.setStatus(true)
                            this.viewCtrl.dismiss({});
                        } else {
                            this._SharedService.presentAlertCustom('ไม่สามารถจัดเก็บข้อมูลลงเครื่องได้ กรุณาตรวจสอบ');
                        }
                    });
                }
            });
        } else {
            this._SharedService.presentAlert();
        }
    }

    dismiss() {
        this.viewCtrl.dismiss({});
    }
}
