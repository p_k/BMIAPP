import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'datePipe'})
export class datePipe implements PipeTransform {
    transform(value: number, exponent: string): any {
      let dateTime = new Date(value)
      return dateTime.getDate() + '-' + (Number(dateTime.getMonth())+1) + '-' + dateTime.getFullYear()
    }
} 