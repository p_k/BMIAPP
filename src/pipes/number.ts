import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'currencyFM'})
export class CurrencyPipe implements PipeTransform {
    transform(value: number, exponent: string): any {
        let curency = (isNaN(Number(value)) ? 0 : Number(value)).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        return curency;
    }
}