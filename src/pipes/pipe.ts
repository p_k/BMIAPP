import { NgModule }      from '@angular/core';
import { CurrencyPipe }  from './number';
import { datePipe } from './date'
 
@NgModule({
    imports:        [],
    declarations:   [CurrencyPipe, datePipe],
    exports:        [CurrencyPipe, datePipe],
})

export class PipeModule {
 
    static forRoot() {
        return {
            ngModule: PipeModule,
            providers: [],
        }; 
    }
}  