import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { Observable } from 'rxjs';
import { bodyJson } from '../pages/home/body-style-data'

@Injectable()
export class CalculatorService {
  private subject : Subject<any> = new Subject();
  private factorCal : any = {
    height: '', 
    weight: ''
  }
  constructor(
    private _bodyJson :bodyJson
  ) {

  } 
  private dataMap = this._bodyJson.data

  get getFactorCal () {
    return this.factorCal
  }

  subscript(): Observable<any> {
    return this.subject.asObservable();
  }

  private mapLevelEmi(val) {
    let data = {
      val : 0 // default value
    };
    this.dataMap.map((obj, i) => {
      if(val <= obj.val && obj.type === 'less' && val > data.val) { data = obj;  }
      if(val >= obj.val && obj.type === 'more') { data = obj;  }
    })
    console.log(data)
    return data;
  }

  private factorReset() {
    this.factorCal.height = ''
    this.factorCal.weight = ''
  }

  public calculatorEmi(param) {
    const { height, weight } = param
    let meter = height / 100;
    let emi = parseFloat((weight / Math.pow(meter, 2)).toFixed(2));

    this.factorReset();
    this.subject.next({
      action: 'changeTab',
      w: weight,
      h: height,
      r: emi,
      ...this.mapLevelEmi(emi)
    });
  }


}