import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { Observable } from 'rxjs';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import 'rxjs/add/operator/toPromise';
import { NativeStorage } from '@ionic-native/native-storage';

@Injectable()
export class requestService {
  private diff : number = 0;
  private subject : Subject<any> = new Subject();
  private statusOnline : boolean = false;
  private profile : any = {};
  constructor(
    private _HTTP : Http,
    private _spinnerDialog: SpinnerDialog,
    private _NativeStorage : NativeStorage,
  ) {

  }

  checkStatusAuth() {

  }

  setProfile(param) {
    this.profile = {
        Title: param.Title,
        Name: param.Name,
        Surname: param.Surname
    }
    this._NativeStorage.setItem('user', {
        Title: param.Title,
        Name: param.Name,
        Surname: param.Surname
    })
    .then(
        (data) => { return true},
        error => false
    );
  }

  logout() {
    this.setProfile({ Title: '', Name: '', Surname: ''});
    this._NativeStorage.remove('headerToken'); // clear header Token
    this._NativeStorage.remove('user'); // clear header Token
    this.setStatus(false)
  }

  setStatus(bool: boolean) {
    this.statusOnline = bool
    return this.subject.next({
        type: 'signin',
        value: this.statusOnline,
        profile: this.profile
    })
  }

  get Status() {
      return this.statusOnline
  }

  subscript(): Observable<any> {
    return this.subject.asObservable();
  }

  checkStatus() {
    this._spinnerDialog.show()
    return this._NativeStorage.getItem('user').then(
        data => {
            this._spinnerDialog.hide();
            this.setProfile(data)
            this.setStatus(true)
        },
        error => {this.logout(); this._spinnerDialog.hide(); }
    ); 
  }

//   async onUpdateAuthToken(token : string = '', username : string = '') { // on update api
//     return await this._NativeStorage.setItem('headerToken', {
//         exp : this.getCurrentDateTime(90),
//         token: token,
//         username : username
//     })
//     .then(
//         (data) => { return true},
//         error => false
//     );
//   }

  async onUpdateAuthToken(token : string = '', username : string = '') { // on update api
    return await this._NativeStorage.setItem('headerToken', {
        exp : this.getCurrentDateTime(90),
        token: token,
        username : username
    })
    .then(
        (data) => { return true},
        error => false
    );
  }

  getDateFormDateTime(dateTime : number) {
    let newDate = new Date(dateTime);
    return newDate.getFullYear() + '/' +
        (newDate.getMonth() + 1) + '/' +
        newDate.getDay() + ' ' +
        newDate.getHours() + ':' +
        newDate.getMinutes() + ':' +
        newDate.getSeconds()
  }

  getCurrentDateTime(cost : number = 0) {
    let date = new Date();
    let minutes = 1000 * 60;
    let hours = minutes * 60;
    let response;
    if(this.diff < 0) {
        date.setTime((date.getTime() - this.diff));
    } else {
        date.setTime((date.getTime() + this.diff));
    }
    response = date.getTime() + (minutes * cost);
    return this.getDateFormDateTime(response);
  }

  checkTokenExpire(exp : string) {
    return ((this.getCurrentDateTime() > exp) ? true : false); // time out
  }

  async loadData(method, url ,params = new FormData(), notCheck = false) {
    this._spinnerDialog.show();
    if(notCheck) {
        return this.request(method, url, params)
    }
    return await this._NativeStorage.getItem('headerToken').then(
        data => {
            this._spinnerDialog.hide();
            params.append('token', data.token)
            return this.request(method, url ,params)
        },
        error => {this.logout(); this._spinnerDialog.hide(); }
    ); 
  } 

  async request(method : string ,url: string, params : any = {}) {
    this._spinnerDialog.show();
     switch (method) {
         case 'get' :
         return await this._HTTP.get(url).toPromise()
         .then((response : Response) => this.extactData(response, this._spinnerDialog))
         .catch((error : Response) => this.errorResponse(error, this._spinnerDialog));
         case 'post' :
         return  await this._HTTP.post(url, params).toPromise()
             .then((response : Response) => this.extactData(response, this._spinnerDialog))
             .catch((error : Response) => this.errorResponse(error, this._spinnerDialog));
         default: break;
     }
  } 

  getHeader() {
    let headers = new Headers();
    headers.append('Content-Type', 'multipart/form-data');
    return  new RequestOptions({
        headers: headers,
    });
  }

  extactData (res, spinner) {
      spinner.hide();
      if(typeof res === 'object' && res.status === 200) {
          let body = res.json();
          return body || {};
      }
  }

  errorResponse(error, spinner) {
      spinner.hide();
      return { error : error.statusText};
  }
}