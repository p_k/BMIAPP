import {AlertController} from 'ionic-angular';
import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {

    constructor(
        private _AlertController : AlertController
    ) {

    }

    presentAlert(condition: number = 0, title: string = 'ข้อผิดพลาด') {
      let msgError = 'กรุณากรอกข้อมูลที่บังคับให้ครบถ้วน';
      switch (condition) {
      case 1: msgError = 'กรุณาระบุจำนวนเงิน'; break;
      case 2: msgError = 'ออกจากระบบสำเร็จ'; break;
      default : break
      }

      // msgError string on alert description
      let alert = this._AlertController.create({
          title: title,
          subTitle: msgError,
          buttons: ['ปิด']
      });
      alert.present();
  }

   //alert model
   presentAlertCustom(txt : string, title: string = 'ข้อผิดพลาด') {
        let msgError = txt;

        // msgError string on alert description
        let alert = this._AlertController.create({
            title: title,
            subTitle: msgError,
            buttons: ['ปิด']
        });
        alert.present();
    }

    presentConfirm(title: string = '', msg: string = '', con: string = '', event : any) {
        let alert = this._AlertController.create({
          title: title,
          message: msg,
          buttons: [
            {
              text: 'ยกเลิก',
              role: 'cancel'
            },
            {
              text: con,
              handler: () => {
                event();
              }
            }
          ]
        });
        alert.present();
      }

}